<?php
require_once "mysql_connection.php";
session_start();

	if (isset($_POST["register"])) 
	{
		# code...
		$staff_name = $_POST["staff_name"];
		$staff_address = $_POST["staff_address"];
		$staff_phone =  $_POST["staff_phone"];
    $staff_email = $_POST["staff_email"];

				
		
		$statement=$connection->prepare("Select * From staff Where  staff_email =?");
		$statement->bind_param("s",$staff_email);
		$statement->execute(); 
		if($statement->fetch()) {
			echo"<script>alert('Email already exist!Choose another email and register again!');</script>";
		}
		
		else {	
		    $statement = $connection ->prepare("Insert Into staff (staff_name,staff_address,staff_phone,staff_email) Values(?,?,?,?)");
		
			$statement ->bind_param("ssss",$staff_name,$staff_address,$staff_phone,$staff_email);
			$statement ->execute();
			if($statement->error) {
				$err=$statement ->error;
				echo"<script>alert('$err');</script>";
			}
			else {
				echo "<script>alert('Success Register!');location.assign('Cu_login.php');</script>";
			}
			$statement ->close();
		}
		
	}
	
?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Ever Green</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
<link rel="stylesheet"  href="css/style1.css" type="text/css" media="screen">
<script src="js/jquery-1.6.2.min.js"></script>
<script src="js/jquery.galleriffic.js"></script>
<script src="js/jquery.opacityrollover.js"></script>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page1">
<!--==============================header=================================-->
<header>
  <div class="row-1">
    <div class="main">
      <div class="container_12">
        <div class="grid_12">
          <nav>
            <ul class="menu">
              <li><a class="active" href="index.html">About Us</a></li>
              <li><a href="services.html">Services</a></li>
              <li><a href="catalogue.html">Catalogue</a></li>
              <li><a href="pricing.html">Pricing</a></li>
              <li><a href="contacts.html">Contacts</a></li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="row-2">
    <div class="main">
      <div class="container_12">
        <div class="grid_9">
          <h1> <a class="logo" href="index.html">Eve<strong>r</strong>Green</a> <span>furniture</span> </h1>
        </div>
        <div class="grid_3">
          <form id="search-form" action="#" method="post" enctype="multipart/form-data">
            <fieldset>
              <div class="search-field">
                <input name="search" type="text">
                <a class="search-button" href="#"><span>search</span></a> </div>
            </fieldset>
          </form>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</header>
<!-- content -->
<section id="content">
  <div class="bg-top">
    <div class="bg-top-2">
      <div class="bg">
        <div class="bg-top-shadow">
          <!-- <div class="main"> -->
            <!-- <div class="gallery p3"> -->
            <div id="registration-form">
	<div class='fieldset'>
    <legend>Staff Register</legend>
		<form action="#" method="post" data-validate="parsley">
			<div class='row'>
				<label for='firstname'>Staff Name</label>
				<input type="text" placeholder="Your Name" name='staff_name' id='firstname' data-required="true" data-error-message="Your First Name is required">
			</div>
			<div class='row'>
				<label for="email">Staff Address</label>
				<input type="text" placeholder="Your Address"  name='staff_address' data-required="true" data-type="email" data-error-message="Your E-mail is required">
			</div>
			<div class='row'>
				<label for="cemail">Enter Your Phone Number</label>
				<input type="text" placeholder="Enter Your Phone Number" name='staff_phone' data-required="true" data-error-message="Your E-mail must correspond">
      </div>
      <div class='row'>
				<label for="cemail">Enter your E-mail</label>
				<input type="text" placeholder="Enter your E-mail" name='staff_email' data-required="true" data-error-message="Your E-mail must correspond">
			</div>
			<input type="submit" value="Register" name="register">
		</form>
	</div>
</div>
              
            </div>
            <div class="container_12">
              <div class="wrapper">
                <article class="grid_12">
                  <h3 class="color-1">Our Services List</h3>
                  <div class="wrapper">
                    <article class="grid_6 alpha">
                      <figure class="img-indent frame"><img src="images/page1-img1.jpg" alt=""></figure>
                      <div class="extra-wrap">
                        <div class="indent-top">
                          <ul class="list-1">
                            <li><a href="#">Interior Decorating Services</a></li>
                            <li class="last"><a href="#">Complete Color <br>
                              Analysis</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </article>
                    <article class="grid_6 omega">
                      <figure class="img-indent frame"><img src="images/page1-img2.jpg" alt=""></figure>
                      <div class="extra-wrap">
                        <div class="indent-top">
                          <ul class="list-1">
                            <li><a href="#">Design Services <br>
                              for Home Construction</a></li>
                            <li class="last"><a href="#">Interior Design Remodeling</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="clear"></div>
                    </article>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bg-bot">
    <div class="main">
      <div class="container_12">
        <div class="wrapper">
          <article class="grid_4">
            <h3 class="prev-indent-bot">About Us</h3>
            <p class="prev-indent-bot">This Interior Design Template goes with two pack ages: with PSD source files and without them.</p>
            PSD source files are available for free for the registered members of Templates.com. The basic package (without PSD source) is available for anyone without registration. </article>
          <article class="grid_4">
            <h3 class="prev-indent-bot">Testimonials</h3>
            <div class="quote">
              <p class="prev-indent-bot">At vero eos et accusamus et iusto odio tium voluptatum deleniti atque corrupti quos<br>
                dolores et quas molestias excepturi sint occaecati cupiditate.</p>
              <h5>James Reese</h5>
              Managing Director </div>
          </article>
          <article class="grid_4">
            <h3 class="prev-indent-bot">What’s New?</h3>
            <time class="tdate-1" datetime="2011-08-15"><a class="link" href="#">15.08.2011</a></time>
            <p class="prev-indent-bot">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.</p>
            <time class="tdate-1" datetime="2011-08-11"><a class="link" href="#">11.08.2011</a></time>
            Totam rem aperiam, eaque ipsa quae ab illo inven tore veritatis et quasi architecto. </article>
        </div>
      </div>
    </div>
  </div>
</section>
<!--==============================footer=================================-->
<footer>
  <div class="main">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_4">
          <div>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</div>
          <div>Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a></div>
          <!-- {%FOOTER_LINK} -->
        </div>
        <div class="grid_4"> <span class="phone-numb"><span>+1(800)</span> 123-1234</span> </div>
        <div class="grid_4">
          <ul class="list-services">
            <li><a href="#"></a></li>
            <li><a class="item-2" href="#"></a></li>
            <li><a class="item-3" href="#"></a></li>
            <li><a class="item-4" href="#"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
<script>
$(window).load(function () {
    // We only want these styles applied when javascript is enabled
    $('div.navigation').css({
        'width': '320px',
        'float': 'right'
    });
    $('div.content').css('display', 'block');

    // Initially set opacity on thumbs and add
    // additional styling for hover effect on thumbs
    var onMouseOutOpacity = 0.5;
    $('#thumbs ul.thumbs li span').opacityrollover({
        mouseOutOpacity: onMouseOutOpacity,
        mouseOverOpacity: 0.0,
        fadeSpeed: 'fast',
        exemptionSelector: '.selected'
    });

    // Initialize Advanced Galleriffic Gallery
    var gallery = $('#thumbs').galleriffic({
        delay: 7000,
        numThumbs: 12,
        preloadAhead: 6,
        enableTopPager: false,
        enableBottomPager: false,
        imageContainerSel: '#slideshow',
        controlsContainerSel: '',
        captionContainerSel: '',
        loadingContainerSel: '',
        renderSSControls: true,
        renderNavControls: true,
        playLinkText: 'Play Slideshow',
        pauseLinkText: 'Pause Slideshow',
        prevLinkText: 'Prev',
        nextLinkText: 'Next',
        nextPageLinkText: 'Next',
        prevPageLinkText: 'Prev',
        enableHistory: true,
        autoStart: 7000,
        syncTransitions: true,
        defaultTransitionDuration: 900,
        onSlideChange: function (prevIndex, nextIndex) {
            // 'this' refers to the gallery, which is an extension of $('#thumbs')
            this.find('ul.thumbs li span').css({
                opacity: 0.5
            })
        },
        onPageTransitionOut: function (callback) {
            this.find('ul.thumbs li span').css({
                display: 'block'
            });
        },
        onPageTransitionIn: function () {
            this.find('ul.thumbs li span').css({
                display: 'none'
            });
        }
    });
});
</script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/parsley.js/1.2.2/parsley.min.js'></script>

  

    <script  src="js/index.js"></script>
</body>
</html>
