<?php


require_once"mysql_connection.php"; 
require_once("furniturecontroller.php");	
session_start();
 
if(!isset($_SESSION["name"])) {
    echo "<script>alert('login Please!.');location.assign('customer_home.php');</script>";
}
if (isset($_POST["btn_confirm"])) 
{
# code...
$customerID=$_SESSION['customerID'];
$furniture_code=$_SESSION['furniture_code'];
$quantity=$_POST["quantity"];

        
$statement = $connection ->prepare("Insert Into order1 (customerID,furniture_code,quantity) Values(?,?,?)");

    $statement ->bind_param("iii",$customerID,$furniture_code,$quantity);
    $statement ->execute();

    if($statement->error)
    {
        $err=$statement ->error;
        echo"<script>alert('$err');</script>";
    }
    else{				
            
        echo "<script>alert('Your Process is Ok! We will send the confirm mail to you!');location.assign('list.php');</script>";
    }
    $statement ->close();
}


if(isset($_GET["oid"])){
$id=$_GET["oid"];
$_SESSION["furniture_code"] = $id;
$statement=$connection->prepare("Select * From furniture where furniture_code=?");
$statement->bind_param("i",$id);
$statement->execute();
$statement->bind_result($furniture_code,$furniture_name,$furniture_size,$furniture_color,$material_type_name,$furniture_description,$furniture_photo,$furniture_quantity);
$statement->fetch();
$statement->close();

}
 

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Interior Design | Services</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     
<script src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/booklist.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page2">
<!--==============================header=================================-->
<header>
  <div class="row-1">
    <div class="main">
      <div class="container_12">
        <div class="grid_12">
          <nav>
            <ul class="menu">
              <li><a href="index.html">About Us</a></li>
              <li><a class="active" href="services.html">Services</a></li>
              <li><a href="catalogue.html">Catalogue</a></li>
              <li><a href="pricing.html">Pricing</a></li>
              <li><a href="contacts.html">Contacts</a></li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="row-2">
    <div class="main">
      <div class="container_12">
        <div class="grid_9">
          <h1> <a class="logo" href="index.html">Int<strong>e</strong>rior</a> <span>Design</span> </h1>
        </div>
        <div class="grid_3">
          <form id="search-form" action="#" method="post" enctype="multipart/form-data">
            <fieldset>
              <div class="search-field">
                <input name="search" type="text">
                <a class="search-button" href="#"><span>search</span></a> </div>
            </fieldset>
          </form>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</header>
<!-- content -->
<section id="content">
  <div class="bg-top">
    <div class="bg-top-2">
      <div class="bg">
        <div class="bg-top-shadow">
          <div class="main">
            <div class="box p3">
              <div class="padding">
                <div class="container_12">
                  <div class="wrapper">
                    <div class="grid_12">
                      <div class="wrapper">
                          <!-- <section> -->
                          
       Hello <?php echo $_SESSION['name'];$_SESSION['customerID'] ?>! Please fill the following!
      <br>
      <form name="frm" method="POST" enctype="multipart/form-data">
      <table width="450px" height="auto"  style=" margin-left:420px;">
      <br>
			<tr>
				<td colspan="2"align="center"><img src="uploads/<?php echo $furniture_photo;?>" style="width: 200px; height: 200px;" />
			</tr>
			<tr>
				<td>Customer Name:</td>
				<td><?php echo $_SESSION['name'];?></td>
			</tr>
			<tr>
				<td>Email:</td>
			    <td><?php echo $_SESSION['email'];?></td>
			</tr>
			<tr>
			
				<td>furniture Name:</td>
				<td><?php echo $furniture_name; ?></td>
			</tr>
			<tr>
			
				<td>Price:</td>
				<td>$<?php echo $furniture_quantity; ?>&nbsp;</td>
			</tr>
			<tr>
				<td>furniture Size:</td>
				<td><?php
				  echo $sizeString = getSizeStringBySizeId($furniture_size);
				?>		
				</td>
			</tr>
			<tr>
				<td>furniture Color:</td>
				<td><?php
				  echo $colorString = getColorStringBySizeId($furniture_color);
				?></td>
			</tr>
      <tr>
				<td>Material:</td>
				<td><?php
				  echo $materialString = getMaterialStringBySizeId($material_type_name);
				?></td>
			</tr>
			<tr>
				<td>Quantity:</td>
				<td><input type="text" name="quantity" value="" required/></td>
			</tr>
      <tr>
				<td colspan="2">
        <input type="submit" value="Confirm" name="btn_confirm"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" value="Cancel" name="cancel" onclick="location.assign('list.php')"/>
        </td>
			</tr>
			</table>
			 </form>  
            </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container_12">
              <div class="wrapper">
                <article class="grid_4">
                  <h3 class="color-1 p2">Consultation</h3>
                  <span class="text-1">Lorem ipsum dolor sit amet, con sectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua enim <br>
                  ad minim veniam.</span> </article>
                <article class="grid_4">
                  <h3 class="color-1 p2">Our Mission</h3>
                  <span class="text-1">Duis aute irure dolor in reprehen derit in voluptate velit esse cillum dolore eu fugiat nulla xcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.</span> </article>
                <article class="grid_4">
                  <h3 class="color-1 indent-bot">Consultation</h3>
                  <form id="subscribe-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                      <div class="subscribe-field">
                        <input type="text">
                      </div>
                      <a class="button" href="#">Subscribe</a>
                    </fieldset>
                  </form>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bg-bot">
    <div class="main">
      <div class="container_12">
        <div class="wrapper">
          <article class="grid_4">
            <div class="indent-right2">
              <h3 class="prev-indent-bot2">Space Planning</h3>
              <ul class="list-2">
                <li><a href="#">Totam rem aperiam eaque ipsa quae abillo</a></li>
                <li><a href="#">Inventore veritatis quasi architecto beatae vitae</a></li>
                <li><a href="#">Nemo enim ipsam voluptatem quia</a></li>
                <li><a href="#">Voluptas sit aspernatur aut odit aut fugit</a></li>
                <li class="last-item"><a href="#">Sed quia consequuntur magni dolores eos</a></li>
              </ul>
            </div>
          </article>
          <article class="grid_8">
            <h3 class="p2">Selection &amp; Installation</h3>
            <div class="wrapper">
              <figure class="img-indent2 frame2"><img src="images/page2-img2.jpg" alt=""></figure>
              <div class="extra-wrap">
                <h6 class="p1">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias.</h6>
                Excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctioam libero tempore cum soluta. </div>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
</section>
<!--==============================footer=================================-->
<footer>
  <div class="main">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_4">
          <div>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</div>
          <div>Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a></div>
          <!-- {%FOOTER_LINK} -->
        </div>
        <div class="grid_4"> <span class="phone-numb"><span>+1(800)</span> 123-1234</span> </div>
        <div class="grid_4">
          <ul class="list-services">
            <li><a href="#"></a></li>
            <li><a class="item-2" href="#"></a></li>
            <li><a class="item-3" href="#"></a></li>
            <li><a class="item-4" href="#"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
</body>
</html>
