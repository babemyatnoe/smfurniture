-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2018 at 10:19 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `furniture`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_code` int(11) NOT NULL,
  `size` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_code`, `size`, `color`) VALUES
(1, 'small', 'yellow'),
(2, 'big', 'grey'),
(3, 'medium', 'red');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE IF NOT EXISTS `color` (
  `colorID` int(11) NOT NULL,
  `furniture_color` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`colorID`, `furniture_color`) VALUES
(1, 'red'),
(2, 'yellow'),
(3, 'green'),
(4, 'blue'),
(5, 'white'),
(6, 'black'),
(7, 'grey'),
(8, 'lightgrey');

-- --------------------------------------------------------

--
-- Table structure for table `confirmreport`
--

CREATE TABLE IF NOT EXISTS `confirmreport` (
  `ID` int(11) NOT NULL,
  `customerID` int(11) NOT NULL,
  `furniture_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customerID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerID`, `name`, `address`, `phone`, `email`) VALUES
(1, 'khine', 'tamwe', '12445', 'khine@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `furniture`
--

CREATE TABLE IF NOT EXISTS `furniture` (
  `furniture_code` int(11) NOT NULL,
  `furniture_name` varchar(255) NOT NULL,
  `furniture_size` varchar(255) NOT NULL,
  `furniture_color` varchar(255) NOT NULL,
  `material_type_name` varchar(255) NOT NULL,
  `furniture_description` varchar(255) NOT NULL,
  `furniture_photo` varchar(255) NOT NULL,
  `furniture_quantity` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `furniture`
--

INSERT INTO `furniture` (`furniture_code`, `furniture_name`, `furniture_size`, `furniture_color`, `material_type_name`, `furniture_description`, `furniture_photo`, `furniture_quantity`) VALUES
(8, 'luxury living room sets', 'small', 'grey', 'plywood', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. ', '7617ATCheapChic_1000x584px6_QTGoldCoast.jpg', 300),
(11, 'Steel and glass table', 'small', 'red', 'leather', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. ', 'narnarayan-steel-furniture-varachha-road-surat-furniture-dealers-496x4pd.jpg', 200),
(12, 'Bed room sets', 'small', 'red', 'plywood', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humle.', 'br_rm_paris6~Sofia-Vergara-Paris-Silver-5-Pc-King-Upholstered-Bedroom.jpg', 400),
(13, 'Bed room sets', 'extralarge', 'blue', 'plywood', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected hu', '0007115_bedroom_sets_320.jpeg', 300),
(15, 'modern wardrobe', 'extralarge', 'blue', 'leather', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. ', 'Moda-Style-6-White-Glass-and-Mirror-with-Bars.jpg', 500),
(16, 'steel dinning table sets', 'extralarge', 'blue', 'wood', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. ', '0_d0dd9fa2.jpg', 200),
(19, 'qwezxc', 'ff', 'lightgrey', 'plywood', 'fqwfwqfqw', '1j+ojl1FOMkX9WypfBe43D6kjfGErRBPmBzJwXs1M3EMoAJtlSEohvJs9vU.png', 111);

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `material_type_id` int(11) NOT NULL,
  `material_type_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`material_type_id`, `material_type_name`) VALUES
(1, 'wood'),
(2, 'sofa'),
(3, 'steel'),
(4, 'leather'),
(5, 'glass'),
(6, 'plastic'),
(7, 'fabric'),
(8, 'plywood');

-- --------------------------------------------------------

--
-- Table structure for table `order1`
--

CREATE TABLE IF NOT EXISTS `order1` (
  `orderID` int(11) NOT NULL,
  `customerID` int(11) NOT NULL,
  `furniture_code` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `orderDate` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order1`
--

INSERT INTO `order1` (`orderID`, `customerID`, `furniture_code`, `quantity`, `orderDate`) VALUES
(1, 1, 11, 3, '0000-00-00'),
(2, 1, 11, 4, '0000-00-00'),
(3, 1, 11, 3, '0000-00-00'),
(4, 1, 11, 2, '0000-00-00'),
(5, 1, 16, 2, '0000-00-00'),
(6, 1, 7, 2, '0000-00-00'),
(7, 1, 13, 2, '2018-04-12'),
(8, 1, 8, 3, '2018-04-12'),
(9, 1, 8, 2, '2018-04-12'),
(10, 1, 16, 3, '2018-04-13'),
(11, 1, 11, 3, '2018-04-13');

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

CREATE TABLE IF NOT EXISTS `size` (
  `sizeID` int(11) NOT NULL,
  `furniture_size` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`sizeID`, `furniture_size`) VALUES
(1, 'small'),
(2, 'medium'),
(3, 'large'),
(4, 'extralarge'),
(5, 'small'),
(6, 'ff');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_code` int(11) NOT NULL,
  `staff_name` varchar(255) NOT NULL,
  `staff_address` varchar(255) NOT NULL,
  `staff_phone` varchar(255) NOT NULL,
  `staff_email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_code`, `staff_name`, `staff_address`, `staff_phone`, `staff_email`) VALUES
(1, 'khine', 'Tamwe', '123', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_code`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`colorID`);

--
-- Indexes for table `confirmreport`
--
ALTER TABLE `confirmreport`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerID`);

--
-- Indexes for table `furniture`
--
ALTER TABLE `furniture`
  ADD PRIMARY KEY (`furniture_code`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`material_type_id`);

--
-- Indexes for table `order1`
--
ALTER TABLE `order1`
  ADD PRIMARY KEY (`orderID`);

--
-- Indexes for table `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`sizeID`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_code` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `colorID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `confirmreport`
--
ALTER TABLE `confirmreport`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `furniture`
--
ALTER TABLE `furniture`
  MODIFY `furniture_code` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `material_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `order1`
--
ALTER TABLE `order1`
  MODIFY `orderID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `size`
--
ALTER TABLE `size`
  MODIFY `sizeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_code` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
