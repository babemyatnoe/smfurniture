<?php

require_once("furniturecontroller.php");
require_once"mysql_connection.php"; 	
session_start();
 
 if(isset($_GET["did"])){
		$id = $_GET["did"];
		$statement=$connection->prepare("Delete From furniture where furniture_code=?");
		$statement->bind_param("i",$id);
		$statement->execute();
		if($statement->error){
		$statement->close();
			echo"<script>alert('Sorry there something wong. Please Try again.');</script>";
		}
		echo"<script>alert('You have been deleted. Your process had been successful!');location.assign('update.php');</script>";
		
	}
 

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Interior Design | Services</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     
<script src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/booklist.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page2">
<!--==============================header=================================-->
<header>
  <div class="row-1">
    <div class="main">
      <div class="container_12">
        <div class="grid_12">
          <nav>
            <ul class="menu">
              <li><a href="index.html">About Us</a></li>
              <li><a class="active" href="services.html">Services</a></li>
              <li><a href="catalogue.html">Catalogue</a></li>
              <li><a href="pricing.html">Pricing</a></li>
              <li><a href="contacts.html">Contacts</a></li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="row-2">
    <div class="main">
      <div class="container_12">
        <div class="grid_9">
          <h1> <a class="logo" href="index.html">Int<strong>e</strong>rior</a> <span>Design</span> </h1>
        </div>
        <div class="grid_3">
          <form id="search-form" action="#" method="post" enctype="multipart/form-data">
            <fieldset>
              <div class="search-field">
                <input name="search" type="text">
                <a class="search-button" href="#"><span>search</span></a> </div>
            </fieldset>
          </form>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</header>
<!-- content -->
<section id="content">
  <div class="bg-top">
    <div class="bg-top-2">
      <div class="bg">
        <div class="bg-top-shadow">
          <div class="main">
            <div class="box p3">
              <div class="padding">
                <div class="container_12">
                  <div class="wrapper">
                    <div class="grid_12">
                      <div class="wrapper">
                          <!-- <section> -->
                      <div class="container-fluid c1">
    <table class="table">
      <thead>
        <tr>
          <th>Name</th>
         
          <th>size</th>
         
          <th>color</th>
         
          <th>material type</th>
         
          <th>description</th>
          
          <th>Price</th>
          
          <th>Image</th>
          <th colspan="2">Action</th>
        </tr>
      </thead>
      <tbody>
      <?php
    
    $result=$connection->query("Select * from furniture");
    while($row=$result->fetch_assoc()){
          $furniture_code = $row["furniture_code"];
          

   ?>
     <tr>
     <td><?php echo $row["furniture_name"];?></td>
    <td><?php echo $row["furniture_size"];?></td>
    <td><?php echo $row["furniture_color"];?></td>
    <td><?php echo $row["material_type_name"];?></td>
     <td><?php echo $row["furniture_description"];?></td>
     <td><?php echo $row["furniture_quantity"];?></td>
     <td><img src="uploads/<?php echo $row["furniture_photo"];?>" style="width: 60px; height: 40px;" /></td>
     <td><button type="button" value="Update" class="btn btn-secondary" name="Update" onclick="location.assign('furnitureUpdate.php?uid=<?php echo $furniture_code;?>')">Update</button></td>
     <td><button type="button" value="Delete" name="Delete" class="btn btn-warning" onclick="location.assign('update.php?did=<?php echo $furniture_code;?>')">Delete</button></td>
    

    </tr>
    <?php } $result->free(); ?>
      </tbody>
    </table>
  </div>


                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container_12">
              <div class="wrapper">
                <article class="grid_4">
                  <h3 class="color-1 p2">Consultation</h3>
                  <span class="text-1">Lorem ipsum dolor sit amet, con sectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua enim <br>
                  ad minim veniam.</span> </article>
                <article class="grid_4">
                  <h3 class="color-1 p2">Our Mission</h3>
                  <span class="text-1">Duis aute irure dolor in reprehen derit in voluptate velit esse cillum dolore eu fugiat nulla xcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.</span> </article>
                <article class="grid_4">
                  <h3 class="color-1 indent-bot">Consultation</h3>
                  <form id="subscribe-form" action="#" method="post" enctype="multipart/form-data">
                    <fieldset>
                      <div class="subscribe-field">
                        <input type="text">
                      </div>
                      <a class="button" href="#">Subscribe</a>
                    </fieldset>
                  </form>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bg-bot">
    <div class="main">
      <div class="container_12">
        <div class="wrapper">
          <article class="grid_4">
            <div class="indent-right2">
              <h3 class="prev-indent-bot2">Space Planning</h3>
              <ul class="list-2">
                <li><a href="#">Totam rem aperiam eaque ipsa quae abillo</a></li>
                <li><a href="#">Inventore veritatis quasi architecto beatae vitae</a></li>
                <li><a href="#">Nemo enim ipsam voluptatem quia</a></li>
                <li><a href="#">Voluptas sit aspernatur aut odit aut fugit</a></li>
                <li class="last-item"><a href="#">Sed quia consequuntur magni dolores eos</a></li>
              </ul>
            </div>
          </article>
          <article class="grid_8">
            <h3 class="p2">Selection &amp; Installation</h3>
            <div class="wrapper">
              <figure class="img-indent2 frame2"><img src="images/page2-img2.jpg" alt=""></figure>
              <div class="extra-wrap">
                <h6 class="p1">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias.</h6>
                Excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctioam libero tempore cum soluta. </div>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
</section>
<!--==============================footer=================================-->
<footer>
  <div class="main">
    <div class="container_12">
      <div class="wrapper">
        <div class="grid_4">
          <div>Copyright &copy; <a href="#">Domain Name</a> All Rights Reserved</div>
          <div>Design by <a target="_blank" href="http://www.templatemonster.com/">TemplateMonster.com</a></div>
          <!-- {%FOOTER_LINK} -->
        </div>
        <div class="grid_4"> <span class="phone-numb"><span>+1(800)</span> 123-1234</span> </div>
        <div class="grid_4">
          <ul class="list-services">
            <li><a href="#"></a></li>
            <li><a class="item-2" href="#"></a></li>
            <li><a class="item-3" href="#"></a></li>
            <li><a class="item-4" href="#"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
</body>
</html>
